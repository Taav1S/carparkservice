
package carparkservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the carparkservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AddParkingSpotResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2017/ws/carpark", "addParkingSpotResponse");
    private final static QName _GetCarParkResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2017/ws/carpark", "getCarParkResponse");
    private final static QName _GetListOfParkingSpotsInCarParkResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2017/ws/carpark", "getListOfParkingSpotsInCarParkResponse");
    private final static QName _GetParkingSpotResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2017/ws/carpark", "getParkingSpotResponse");
    private final static QName _AddCarParkResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2017/ws/carpark", "addCarParkResponse");
    private final static QName _AddParkingSpotToCarParkResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2017/ws/carpark", "addParkingSpotToCarParkResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: carparkservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ParkingSpotType }
     * 
     */
    public ParkingSpotType createParkingSpotType() {
        return new ParkingSpotType();
    }

    /**
     * Create an instance of {@link CarParkType }
     * 
     */
    public CarParkType createCarParkType() {
        return new CarParkType();
    }

    /**
     * Create an instance of {@link ListOfParkingSpotsInCarParkType }
     * 
     */
    public ListOfParkingSpotsInCarParkType createListOfParkingSpotsInCarParkType() {
        return new ListOfParkingSpotsInCarParkType();
    }

    /**
     * Create an instance of {@link GetListOfParkingSpotsRequest }
     * 
     */
    public GetListOfParkingSpotsRequest createGetListOfParkingSpotsRequest() {
        return new GetListOfParkingSpotsRequest();
    }

    /**
     * Create an instance of {@link AddParkingSpotToCarParkRequest }
     * 
     */
    public AddParkingSpotToCarParkRequest createAddParkingSpotToCarParkRequest() {
        return new AddParkingSpotToCarParkRequest();
    }

    /**
     * Create an instance of {@link AddParkingSpotRequest }
     * 
     */
    public AddParkingSpotRequest createAddParkingSpotRequest() {
        return new AddParkingSpotRequest();
    }

    /**
     * Create an instance of {@link GetCarParkRequest }
     * 
     */
    public GetCarParkRequest createGetCarParkRequest() {
        return new GetCarParkRequest();
    }

    /**
     * Create an instance of {@link GetParkingSpotRequest }
     * 
     */
    public GetParkingSpotRequest createGetParkingSpotRequest() {
        return new GetParkingSpotRequest();
    }

    /**
     * Create an instance of {@link GetListOfCarParksRequest }
     * 
     */
    public GetListOfCarParksRequest createGetListOfCarParksRequest() {
        return new GetListOfCarParksRequest();
    }

    /**
     * Create an instance of {@link GetListOfParkingSpotsResponse }
     * 
     */
    public GetListOfParkingSpotsResponse createGetListOfParkingSpotsResponse() {
        return new GetListOfParkingSpotsResponse();
    }

    /**
     * Create an instance of {@link AddCarParkRequest }
     * 
     */
    public AddCarParkRequest createAddCarParkRequest() {
        return new AddCarParkRequest();
    }

    /**
     * Create an instance of {@link GetListOfParkingSpotsInCarParkRequest }
     * 
     */
    public GetListOfParkingSpotsInCarParkRequest createGetListOfParkingSpotsInCarParkRequest() {
        return new GetListOfParkingSpotsInCarParkRequest();
    }

    /**
     * Create an instance of {@link GetListOfCarParksResponse }
     * 
     */
    public GetListOfCarParksResponse createGetListOfCarParksResponse() {
        return new GetListOfCarParksResponse();
    }

    /**
     * Create an instance of {@link ParkingSpotInCarParkType }
     * 
     */
    public ParkingSpotInCarParkType createParkingSpotInCarParkType() {
        return new ParkingSpotInCarParkType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParkingSpotType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2017/ws/carpark", name = "addParkingSpotResponse")
    public JAXBElement<ParkingSpotType> createAddParkingSpotResponse(ParkingSpotType value) {
        return new JAXBElement<ParkingSpotType>(_AddParkingSpotResponse_QNAME, ParkingSpotType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CarParkType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2017/ws/carpark", name = "getCarParkResponse")
    public JAXBElement<CarParkType> createGetCarParkResponse(CarParkType value) {
        return new JAXBElement<CarParkType>(_GetCarParkResponse_QNAME, CarParkType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListOfParkingSpotsInCarParkType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2017/ws/carpark", name = "getListOfParkingSpotsInCarParkResponse")
    public JAXBElement<ListOfParkingSpotsInCarParkType> createGetListOfParkingSpotsInCarParkResponse(ListOfParkingSpotsInCarParkType value) {
        return new JAXBElement<ListOfParkingSpotsInCarParkType>(_GetListOfParkingSpotsInCarParkResponse_QNAME, ListOfParkingSpotsInCarParkType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParkingSpotType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2017/ws/carpark", name = "getParkingSpotResponse")
    public JAXBElement<ParkingSpotType> createGetParkingSpotResponse(ParkingSpotType value) {
        return new JAXBElement<ParkingSpotType>(_GetParkingSpotResponse_QNAME, ParkingSpotType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CarParkType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2017/ws/carpark", name = "addCarParkResponse")
    public JAXBElement<CarParkType> createAddCarParkResponse(CarParkType value) {
        return new JAXBElement<CarParkType>(_AddCarParkResponse_QNAME, CarParkType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParkingSpotInCarParkType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2017/ws/carpark", name = "addParkingSpotToCarParkResponse")
    public JAXBElement<ParkingSpotInCarParkType> createAddParkingSpotToCarParkResponse(ParkingSpotInCarParkType value) {
        return new JAXBElement<ParkingSpotInCarParkType>(_AddParkingSpotToCarParkResponse_QNAME, ParkingSpotInCarParkType.class, null, value);
    }

}
