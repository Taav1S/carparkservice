
package carparkservice;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="token" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="carParkName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="address" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="owner" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="surveillance" type="{http://www.ttu.ee/idu0075/2017/ws/carpark}surveillanceType"/>
 *         &lt;element name="parkingFacility" type="{http://www.ttu.ee/idu0075/2017/ws/carpark}parkingFacilityType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "token",
    "id",
    "carParkName",
    "address",
    "owner",
    "surveillance",
    "parkingFacility"
})
@XmlRootElement(name = "addCarParkRequest")
public class AddCarParkRequest {

    @XmlElement(required = true)
    protected String token;
    @XmlElement(required = true)
    protected BigInteger id;
    @XmlElement(required = true)
    protected String carParkName;
    @XmlElement(required = true)
    protected String address;
    @XmlElement(required = true)
    protected String owner;
    @XmlElement(required = true)
    protected String surveillance;
    @XmlElement(required = true)
    protected String parkingFacility;

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setId(BigInteger value) {
        this.id = value;
    }

    /**
     * Gets the value of the carParkName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarParkName() {
        return carParkName;
    }

    /**
     * Sets the value of the carParkName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarParkName(String value) {
        this.carParkName = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress(String value) {
        this.address = value;
    }

    /**
     * Gets the value of the owner property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Sets the value of the owner property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwner(String value) {
        this.owner = value;
    }

    /**
     * Gets the value of the surveillance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSurveillance() {
        return surveillance;
    }

    /**
     * Sets the value of the surveillance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSurveillance(String value) {
        this.surveillance = value;
    }

    /**
     * Gets the value of the parkingFacility property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParkingFacility() {
        return parkingFacility;
    }

    /**
     * Sets the value of the parkingFacility property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParkingFacility(String value) {
        this.parkingFacility = value;
    }

}
