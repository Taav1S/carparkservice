
package carparkservice;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="token" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="carParkId" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="parkingSpotId" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="hours" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="hourPrice" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "token",
    "carParkId",
    "parkingSpotId",
    "hours",
    "hourPrice"
})
@XmlRootElement(name = "addParkingSpotToCarParkRequest")
public class AddParkingSpotToCarParkRequest {

    @XmlElement(required = true)
    protected String token;
    @XmlElement(required = true)
    protected BigInteger carParkId;
    @XmlElement(required = true)
    protected BigInteger parkingSpotId;
    protected double hours;
    protected double hourPrice;

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Gets the value of the carParkId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCarParkId() {
        return carParkId;
    }

    /**
     * Sets the value of the carParkId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCarParkId(BigInteger value) {
        this.carParkId = value;
    }

    /**
     * Gets the value of the parkingSpotId property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getParkingSpotId() {
        return parkingSpotId;
    }

    /**
     * Sets the value of the parkingSpotId property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setParkingSpotId(BigInteger value) {
        this.parkingSpotId = value;
    }

    /**
     * Gets the value of the hours property.
     * 
     */
    public double getHours() {
        return hours;
    }

    /**
     * Sets the value of the hours property.
     * 
     */
    public void setHours(double value) {
        this.hours = value;
    }

    /**
     * Gets the value of the hourPrice property.
     * 
     */
    public double getHourPrice() {
        return hourPrice;
    }

    /**
     * Sets the value of the hourPrice property.
     * 
     */
    public void setHourPrice(double value) {
        this.hourPrice = value;
    }

}
