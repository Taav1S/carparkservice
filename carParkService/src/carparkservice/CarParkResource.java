package carparkservice;


import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import javax.enterprise.context.RequestScoped;
import java.math.BigInteger;


/**
 * REST Web Service
 *
 * @author IT134LAPTOP
 */
@Path("carparks")
@RequestScoped
public class CarParkResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of InvoicesResource
     */
    public CarParkResource() {
    }

    /**
     * Retrieves representation of an instance of invoice.InvoicesResource
     *
     * @param token
     * @param hasRelatedParkingSpots
     * @param surveillance
     * @param parkingFacility
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/json")
    public GetListOfCarParksResponse getListOfCarParks(@QueryParam("token") String token,
                                                       @QueryParam("parkingspots") String hasRelatedParkingSpots,
                                                       @QueryParam("surveillance") String surveillance,
                                                       @QueryParam("parkingfacility") String parkingFacility
    ) {

        if ((surveillance.equalsIgnoreCase("yes") || surveillance.equalsIgnoreCase("no")) &&
                (parkingFacility.equalsIgnoreCase("single level") || parkingFacility.equalsIgnoreCase("multilevel"))) {
            CarParkPortType ws = new CarParkPortType();
            GetListOfCarParksRequest request = new GetListOfCarParksRequest();
            request.setToken(token);
            request.setHasRelatedParkingSpots(hasRelatedParkingSpots);
            request.setParkingFacility(parkingFacility);
            request.setSurveillance(surveillance);
            return ws.getListOfCarParks(request);
        } else {
            return new GetListOfCarParksResponse();
        }


    }

    @GET
    @Path("{id : \\d+}") //support digit only
    @Produces("application/json")
    public CarParkType getCarPark(@PathParam("id") String id,
                                  @QueryParam("token") String token) {
        CarParkPortType ws = new CarParkPortType();
        GetCarParkRequest request = new GetCarParkRequest();
        request.setId(BigInteger.valueOf(Integer.parseInt(id)));
        request.setToken(token);
        return ws.getCarPark(request);
    }

    /**
     * @param content
     * @param token
     * @return
     */
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public CarParkType addCarPark(CarParkType content,
                                  @QueryParam("token") String token) {
        CarParkPortType ws = new CarParkPortType();
        AddCarParkRequest request = new AddCarParkRequest();
        if ((content.getParkingFacility().equalsIgnoreCase("multilevel") || content.getParkingFacility().equalsIgnoreCase("single level"))
                && (content.getSurveillance().equalsIgnoreCase("yes") || content.getSurveillance().equalsIgnoreCase("no"))) {
            request.setToken(token);
            request.setAddress(content.getAddress());
            request.setCarParkName(content.getCarParkName());
            request.setOwner(content.getOwner());
            request.setSurveillance(content.getSurveillance());
            request.setParkingFacility(content.getParkingFacility());
            request.setId(content.getId());
            return ws.addCarPark(request);
        } else {
            return new CarParkType();
        }
    }

    @GET
    @Path("{id : \\d+}/parkingspots") //support digit only
    @Produces("application/json")
    public ListOfParkingSpotsInCarParkType getListOfParkingSpotsInCarPark(@PathParam("id") String id,
                                                                          @QueryParam("token") String token) {
        CarParkPortType ws = new CarParkPortType();
        GetListOfParkingSpotsInCarParkRequest request = new GetListOfParkingSpotsInCarParkRequest();
        request.setCarParkId(BigInteger.valueOf(Integer.parseInt(id)));
        request.setToken(token);
        return ws.getListOfParkingSpotsInCarPark(request);
    }

    /**
     * @param content
     * @param token
     * @param id
     * @return
     */
    @POST
    @Path("{id : \\d+}/parkingspots") //support digit only
    @Consumes("application/json")
    @Produces("application/json")
    public ParkingSpotInCarParkType addParkingSpotToCarPark(AddParkingSpotToCarParkRequest content,
                                                            @QueryParam("token") String token,
                                                            @PathParam("id") String id) {
        CarParkPortType ws = new CarParkPortType();
        AddParkingSpotToCarParkRequest request = new AddParkingSpotToCarParkRequest();
        request.setToken(token);
        request.setCarParkId(BigInteger.valueOf(Integer.parseInt(id)));
        request.setParkingSpotId(content.getParkingSpotId());
        request.setHours(content.getHours());
        request.setHourPrice(content.getHourPrice());
        return ws.addParkingSpotToCarPark(request);
    }

}
