package carparkservice;

import java.math.BigInteger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

/**
 * REST Web Service
 *
 * @author Admin
 */
@Path("parkingspots")
public class ParkingSpotResource {

    @Context
    private UriInfo context;

    public ParkingSpotResource() {
    }

    @GET
    @Produces("application/json")
    public GetListOfParkingSpotsResponse getListOfParkingSpots(@QueryParam("token") String token) {
        CarParkPortType ws = new CarParkPortType();
        GetListOfParkingSpotsRequest request = new GetListOfParkingSpotsRequest();
        request.setToken(token);
        return ws.getListOfParkingSpots(request);
    }

    @GET
    @Path("{id : \\d+}") //supports digits only
    @Produces("application/json")
    public ParkingSpotType getParkingSpot(@PathParam("id") String id,
                                          @QueryParam("token") String token) {
        CarParkPortType ws = new CarParkPortType();
        GetParkingSpotRequest request = new GetParkingSpotRequest();
        request.setId(BigInteger.valueOf(Integer.parseInt(id)));
        request.setToken(token);
        return ws.getParkingSpot(request);
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public ParkingSpotType addParkingSpots(ParkingSpotType content,
                                           @QueryParam("token") String token) {

        if (content.getIsAvailable().equalsIgnoreCase("yes") || content.getIsAvailable().equalsIgnoreCase("no")) {

            CarParkPortType ws = new CarParkPortType();
            AddParkingSpotRequest request = new AddParkingSpotRequest();
            request.setSpotCode(content.getSpotCode());
            request.setIsAvailable(content.getIsAvailable());
            request.setId(content.getId());
            request.setToken(token);
            return ws.addParkingSpot(request);
        }
        else return new ParkingSpotType();
    }
}
