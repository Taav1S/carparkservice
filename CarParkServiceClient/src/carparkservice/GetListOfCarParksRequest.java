
package carparkservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="token" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="surveillance" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="parkingFacility" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="hasRelatedParkingSpots" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "token",
    "surveillance",
    "parkingFacility",
    "hasRelatedParkingSpots"
})
@XmlRootElement(name = "getListOfCarParksRequest")
public class GetListOfCarParksRequest {

    @XmlElement(required = true)
    protected String token;
    @XmlElement(required = true)
    protected String surveillance;
    @XmlElement(required = true)
    protected String parkingFacility;
    protected String hasRelatedParkingSpots;

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Gets the value of the surveillance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSurveillance() {
        return surveillance;
    }

    /**
     * Sets the value of the surveillance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSurveillance(String value) {
        this.surveillance = value;
    }

    /**
     * Gets the value of the parkingFacility property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParkingFacility() {
        return parkingFacility;
    }

    /**
     * Sets the value of the parkingFacility property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParkingFacility(String value) {
        this.parkingFacility = value;
    }

    /**
     * Gets the value of the hasRelatedParkingSpots property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHasRelatedParkingSpots() {
        return hasRelatedParkingSpots;
    }

    /**
     * Sets the value of the hasRelatedParkingSpots property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHasRelatedParkingSpots(String value) {
        this.hasRelatedParkingSpots = value;
    }

}
