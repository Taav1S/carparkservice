
package carparkservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for parkingSpotInCarParkType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="parkingSpotInCarParkType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="parkingSpot" type="{http://www.ttu.ee/idu0075/2017/ws/carpark}parkingSpotType"/>
 *         &lt;element name="maxParkingTimeInHours" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="oneHourPrice" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "parkingSpotInCarParkType", propOrder = {
    "parkingSpot",
    "maxParkingTimeInHours",
    "oneHourPrice"
})
public class ParkingSpotInCarParkType {

    @XmlElement(required = true)
    protected ParkingSpotType parkingSpot;
    protected double maxParkingTimeInHours;
    protected double oneHourPrice;

    /**
     * Gets the value of the parkingSpot property.
     * 
     * @return
     *     possible object is
     *     {@link ParkingSpotType }
     *     
     */
    public ParkingSpotType getParkingSpot() {
        return parkingSpot;
    }

    /**
     * Sets the value of the parkingSpot property.
     * 
     * @param value
     *     allowed object is
     *     {@link ParkingSpotType }
     *     
     */
    public void setParkingSpot(ParkingSpotType value) {
        this.parkingSpot = value;
    }

    /**
     * Gets the value of the maxParkingTimeInHours property.
     * 
     */
    public double getMaxParkingTimeInHours() {
        return maxParkingTimeInHours;
    }

    /**
     * Sets the value of the maxParkingTimeInHours property.
     * 
     */
    public void setMaxParkingTimeInHours(double value) {
        this.maxParkingTimeInHours = value;
    }

    /**
     * Gets the value of the oneHourPrice property.
     * 
     */
    public double getOneHourPrice() {
        return oneHourPrice;
    }

    /**
     * Sets the value of the oneHourPrice property.
     * 
     */
    public void setOneHourPrice(double value) {
        this.oneHourPrice = value;
    }

}
