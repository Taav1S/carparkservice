package example;

import carparkservice.AddParkingSpotRequest;
import carparkservice.CarParkPortTypeService;
import carparkservice.GetParkingSpotRequest;
import carparkservice.ParkingSpotType;

import java.math.BigInteger;

class CarParkServiceClient {
    public static void main(String[] argv) {
        carparkservice.CarParkPortType service = new CarParkPortTypeService().getCarParkPortTypePort();

        AddParkingSpotRequest parkingSpotRequest = new AddParkingSpotRequest();
        parkingSpotRequest.setId(BigInteger.ONE);
        parkingSpotRequest.setIsAvailable("no");
        parkingSpotRequest.setSpotCode("1a");
        parkingSpotRequest.setToken("salajane");

        service.addParkingSpot(parkingSpotRequest);

        GetParkingSpotRequest parkingSpotGetRequest = new GetParkingSpotRequest();
        parkingSpotGetRequest.setId(BigInteger.ONE);
        parkingSpotGetRequest.setToken("salajane");

        ParkingSpotType getParkingSpot = service.getParkingSpot(parkingSpotGetRequest);

        System.out.println("Id: " + getParkingSpot.getId() +
                ", isAvailable: " + getParkingSpot.getIsAvailable() + ", spotCode: " + getParkingSpot.getSpotCode());
    }
}
